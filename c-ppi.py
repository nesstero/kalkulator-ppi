#! /usr/bin/env python
import argparse
from math import sqrt

class calPpi():

    def __init__(self):
        arg = argparse.ArgumentParser()
        arg.add_argument("-l", "--width", required=True, help="lebar resolusi dalam ukuran pixels")
        arg.add_argument("-t", "--height", required=True, help="tingi resolusi dalam ukuran pixels")
        arg.add_argument("-s", "--screen", required=True, help="ukuran layar dalam inches")
        args = vars(arg.parse_args())
        self.lebar = int(args['width'])
        self.tingi = int(args['height'])
        self.layar = float(args['screen'])

    def dpi_ppi(self):
        dp = sqrt(self.lebar ** 2 + self.tingi ** 2)
        di  = self.layar
        ppi = dp / di
        ppi = round(ppi)
        dpi = 1 / ppi
        dpi_b = round(dpi, 4)
        dpi_mm = dpi * 25.4
        dpi_mm_b = round(dpi_mm, 4)
        return ppi, dpi_b, dpi_mm_b, self.lebar, self.tingi, di

ppi = calPpi()
ppi = ppi.dpi_ppi()
print(f"""
=============== Kalkulator PPI ==============
 Resolusi (dalam pixels) = {ppi[3]} x {ppi[4]}
 Screen (dalam inches)   = {ppi[5]}
 PPI (Pixels Per Inches) = {ppi[0]}
 DPI (Dot Pitch)         = {ppi[1]} ({ppi[2]}mm)
=============================================
""")
