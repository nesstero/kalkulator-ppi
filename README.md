# Kalkulator PPI
Kalkulator untuk menghitung PPI (Pixel Per Inches) dan DPI (Dot Pitch)

# Menggunakan kalkulator ppi
```
$ git clone git@gitlab.com:nesstero/kalkulator-ppi.git
$ cd kalkulator-ppi
$ python c-ppi.py -h
$ python c-ppi.py -l 1600 -t 900 -s 13.3
```
Output
```
=============== Kalkulator PPI ==============
 Resolusi (dalam pixels) = 1600 x 900
 Screen (dalam inches)   = 13.3
 PPI (Pixels Per Inches) = 138
 DPI (Dot Pitch)         = 0.0072 (0.1841mm)
=============================================
```
